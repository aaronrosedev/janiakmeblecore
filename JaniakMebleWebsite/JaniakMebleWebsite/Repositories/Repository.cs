﻿using JaniakMebleWebsite.Interfaces.Repositories;
using JaniakMebleWebsite.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JaniakMebleWebsite.Repositories
{
    public class Repository : DataBaseAccess, IRepository
    {

        private const string WEBSITE_URL = "";// "http://janiakmeble.dosomething.pl";

        private Lazy<List<Category>> _categoriesCache;
        private Lazy<List<Subcategory>> _subcategoriesCache;
        private Lazy<List<Department>> _departmentsCache;
        private Lazy<List<Product>> _productsCache;
        private Lazy<List<Textile>> _textiliesCache;
        private Lazy<List<Inspiration>> _inspirationsCache;
        private Lazy<List<Information>> _informationsCache;
        private Lazy<List<Baner>> _banersCache;
        private Lazy<List<QuestionAndAnswer>> _questionAndAnswerCache;
        private Lazy<List<string>> _slogansCache;


        public Repository()
        {
            PrepareCache();
        }

        public void PrepareCache()
        {
            _categoriesCache = new Lazy<List<Category>>(GetCategoriesFromDb);
            _subcategoriesCache = new Lazy<List<Subcategory>>(GetSubcategoriesFromDb);
            _departmentsCache = new Lazy<List<Department>>(GetDepartmentsFromDb);
            _productsCache = new Lazy<List<Product>>(GetProductsFromDb);
            _textiliesCache = new Lazy<List<Textile>>(GetTextiliesFromDb);
            _inspirationsCache = new Lazy<List<Inspiration>>(GetInspirationsFromDb);
            _informationsCache = new Lazy<List<Information>>(GetInformationsFromDb);
            _banersCache = new Lazy<List<Baner>>(GetBanersFromDb);
            _questionAndAnswerCache = new Lazy<List<QuestionAndAnswer>>(GetQuestionAndAnswerFromDb);
            _slogansCache = new Lazy<List<string>>(GetSlogansFromDb);
        }

        public void RefreshCache()
        {
            var a1 = _categoriesCache.Value.ToList();
            var a2 = _subcategoriesCache.Value.ToList();
            var a3 = _departmentsCache.Value.ToList();
            var a4 = _productsCache.Value.ToList();
            var a5 = _textiliesCache.Value.ToList();
            var a6 = _inspirationsCache.Value.ToList();
            var a7 = _informationsCache.Value.ToList();
            var a8 = _banersCache.Value.ToList();
            var a9 = _questionAndAnswerCache.Value.ToList();
            var a10 = _slogansCache.Value.ToList();
        }



        public IEnumerable<Category> GetCategories(int departmentId)
        {
            return _categoriesCache.Value.Where(A => A.Department.Id == departmentId);
        }

        public Category GetCategory(int categoryId)
        {
            return _categoriesCache.Value.FirstOrDefault(A => A.Id == categoryId);
        }

        public IEnumerable<Product> GetProducts(string category)
        {
            return _productsCache.Value.Where(A => A.Category.SeoUrl == category && A.MainProduct == 0);
        }

        public IEnumerable<Product> GetProducts(int category)
        {
            return _productsCache.Value.Where(A => A.Category.Id == category && A.MainProduct == 0);
        }

        public Product GetProduct(string product)
        {
            return _productsCache.Value.FirstOrDefault(A => A.SeoUrl == product);
        }

        public Product GetProduct(int product)
        {
            return _productsCache.Value.FirstOrDefault(A => A.Id == product);
        }

        public IEnumerable<Inspiration> GetInspirations()
        {
            return _inspirationsCache.Value;
        }

        public IEnumerable<Information> GetInformations()
        {
            return _informationsCache.Value;
        }

        public IEnumerable<Baner> GetBaners()
        {
            return _banersCache.Value;
        }

        public IEnumerable<QuestionAndAnswer> GetQuestionAndAnswers()
        {
            return _questionAndAnswerCache.Value;
        }

        public IEnumerable<string> GetSlogans()
        {
            return _slogansCache.Value;
        }

        private List<string> GetSlogansFromDb()
        {
            var result = new List<string>();

            string query = "SELECT * FROM slogans";         
            connection.Open();
            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(reader.GetString("slogan"));
                }
            }
            connection.Close();

            return result;
        }

        private List<Category> GetCategoriesFromDb()
        {
            var result = new List<Category>();

            string query = "SELECT * FROM categories";
            var departments = _departmentsCache.Value;
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Category
                    {
                        Id = reader.GetInt32("id"),
                        SeoUrl = reader.GetString("seo_url"),
                        Name = reader.GetString("name"),
                        Prefix = reader.GetString("prefix"),
                        Department = departments.First(A => A.Id == reader.GetInt32("department")),
                        Photo = WEBSITE_URL + reader.GetString("photo")
                    });
                }
            }
            connection.Close();

            return result;
        }

        private List<QuestionAndAnswer> GetQuestionAndAnswerFromDb()
        {
            var result = new List<QuestionAndAnswer>();

            string query = "SELECT * FROM questions_and_answers";
            var departments = _departmentsCache.Value;
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new QuestionAndAnswer
                    {
                        Question = reader.GetString("question"),
                        Answer = reader.GetString("answer")                      
                    });
                }
            }
            connection.Close();

            return result;
        }

        private List<Subcategory> GetSubcategoriesFromDb()
        {
            var result = new List<Subcategory>();

            string query = "SELECT * FROM subcategories";
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Subcategory
                    {
                        Id = reader.GetInt32("id"),
                        Name = reader.GetString("name")
                    });
                }
            }
            connection.Close();
            return result;
        }

        private List<Department> GetDepartmentsFromDb()
        {
            var result = new List<Department>();

            string query = "SELECT * FROM departments";
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Department
                    {
                        Id = reader.GetInt32("id"),
                        Name = reader.GetString("name")
                    });
                }
            }
            connection.Close();
            return result;
        }

        private List<Inspiration> GetInspirationsFromDb()
        {
            var result = new List<Inspiration>();

            string query = "SELECT * FROM inspirations ORDER BY id DESC";
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Inspiration
                    {
                        Name = reader.GetString("name"),
                        Description = reader.GetString("description"),
                        Url = WEBSITE_URL +  reader.GetString("url")
                    });
                }
            }
            connection.Close();
            return result;
        }

        private List<Information> GetInformationsFromDb()
        {
            var result = new List<Information>();

            string query = "SELECT * FROM informations";
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Information
                    {
                        Header = reader.GetString("header"),
                        Content = reader.GetString("content"),
                        SeoUrl = reader.GetString("seo_url")
                    });
                }
            }
            connection.Close();
            return result;
        }

        private List<Baner> GetBanersFromDb()
        {
            var result = new List<Baner>();

            string query = "SELECT * FROM baners";
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Baner
                    {
                        Header = reader.GetString("header"),
                        Content = reader.GetString("content"),
                        RedirectUrl = reader.GetString("redirect_url"),
                        RedirectText = reader.GetString("redirect_text"),
                        PhotoUrl = WEBSITE_URL + reader.GetString("photo_url")
                    });
                }
            }
            connection.Close();
            return result;
        }

        private List<Textile> GetTextiliesFromDb()
        {
            var result = new List<Textile>();

            string query = "SELECT * FROM textilies";
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Textile
                    {
                        Id = reader.GetInt32("id"),
                        Name = reader.GetString("name"),
                        Description = reader.GetString("description"),
                        Url = reader.GetString("url")
                    });
                }
            }
            connection.Close();
            return result;
        }

        private List<Product> GetProductsFromDb()
        {
            var result = new List<Product>();
            var categories = _categoriesCache.Value;
            var subcategories = _subcategoriesCache.Value;
            var textilies = _textiliesCache.Value;
            string query = "SELECT * FROM products ORDER BY main_product";
            connection.Open();

            var cmd = new MySqlCommand(query, connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Product
                    {
                        Id = reader.GetInt32("id"),
                        SeoUrl = reader.GetString("seo_url"),
                        Name = reader.GetString("name"),
                        Description = reader.GetString("description").Replace("\r\n", "<br />"),
                        Price = reader.GetDouble("price"),
                        MainProduct = !reader.IsDBNull(reader.GetOrdinal("main_product")) ? reader.GetInt32("main_product") : 0,
                        Category = categories.First(A => A.Id == reader.GetInt32("category")),
                        Subcategory = !reader.IsDBNull(reader.GetOrdinal("subcategory")) ? subcategories.FirstOrDefault(A => A.Id == (reader.GetInt32("subcategory"))) : null,
                        Attributes = new List<string>(),
                        Photos = new List<string>(),
                        Textilies = new List<Textile>(),
                        Sizes = new Dictionary<string, string>(),
                        Parameters = new Dictionary<string, string>()
                    });
                }
            }

            cmd = new MySqlCommand("SELECT * FROM sizes s ORDER BY s.product, s.key", connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.First(A => A.Id == reader.GetInt32("product")).Sizes.Add(reader.GetString("key"), reader.GetString("value"));
                }
            }

            cmd = new MySqlCommand("SELECT * FROM parameters p ORDER BY p.product, p.key", connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.First(A => A.Id == reader.GetInt32("product")).Parameters.Add(reader.GetString("key"), reader.GetString("value"));
                }
            }

            cmd = new MySqlCommand("SELECT * FROM product_photos ORDER BY product, position ", connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.First(A => A.Id == reader.GetInt32("product")).Photos.Add(WEBSITE_URL + reader.GetString("url"));
                }
            }

            cmd = new MySqlCommand("SELECT * FROM attributes", connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.First(A => A.Id == reader.GetInt32("product")).Attributes.Add(reader.GetString("attribute"));
                }
            }

            cmd = new MySqlCommand("SELECT * FROM product_textilies ORDER BY product", connection);
            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.First(A => A.Id == reader.GetInt32("product")).Textilies.Add(textilies.First(A => A.Id == reader.GetInt32("textile")));
                }
            }

            connection.Close();

            
            return FixVariantsInProducts(result).ToList();
        }

        private IEnumerable<Product> FixVariantsInProducts(IEnumerable<Product> input)
        {
            var mainProducts = input.Where(A => A.MainProduct == 0);
            foreach (var mainProduct in mainProducts) {
                foreach (var variant in input.Where(A => A.MainProduct == mainProduct.Id))
                {
                    mainProduct.Variants.Add(variant);                   
                }
            }
            foreach (var mainProduct in mainProducts)
            {
                foreach (var variant in input.Where(A => A.MainProduct == mainProduct.Id))
                {
                    var variantsOfvariant = mainProduct.Variants.Where(A => A.MainProduct == mainProduct.Id && A.Id != variant.Id);
                    variant.Variants.Add(mainProduct);
                    variant.Variants.AddRange(variantsOfvariant);
                }
            }

            return input;
        }

        
    }
}
