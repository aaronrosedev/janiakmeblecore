﻿using JaniakMebleWebsite.Interfaces.Repositories;
using JaniakMebleWebsite.Interfaces.Services;
using JaniakMebleWebsite.Models;
using System.Collections.Generic;

namespace JaniakMebleWebsite.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository _repository;



        public CategoryService(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Category> GetAllCategories(int departmentId)
        {
            return _repository.GetCategories(departmentId);
        }
    }
}
