﻿using JaniakMebleWebsite.Interfaces.Services;
using JaniakMebleWebsite.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace JaniakMebleWebsite.Services
{
    public class MailService : IMailService
    {
        public void SendEmailToCustomer(ContactFormModel contactForm)
        {
      
            MailAddress from = new MailAddress("biuro@janiakmeble.pl");
         
            var message = GenerateMessageBodyToCustomer(contactForm);

            MailMessage mail = new MailMessage();

            var smtp = new SmtpClient
            {
                Host = "janiakmeble.nazwa.pl",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("biuro@janiakmeble.nazwa.pl", "D26cZWAX")
            };

            mail.From = from;
            mail.To.Add(contactForm.Email);
            mail.Subject = "Zapytanie do firmy Janiak-Meble";
            mail.Body = message;
            mail.IsBodyHtml = true;
            smtp.Send(mail);
        }

        private static string GenerateMessageBodyToCustomer(ContactFormModel contactForm)
        {
            var name = !string.IsNullOrWhiteSpace(contactForm.Name) ? $"Imię: {contactForm.Name}<br />" : "";
            var email = !string.IsNullOrWhiteSpace(contactForm.Email) ? $"Adres email: {contactForm.Email}<br />" : "";
            var phone = !string.IsNullOrWhiteSpace(contactForm.Phone) ? $"Numer telefonu: {contactForm.Phone}<br />" : "";

            return $@"<p><a href=""https://janiakmeble.pl""><img style=""display: block; margin-left: auto; margin-right: auto;"" src=""https://janiakmeble.pl/img/logo-min.png"" alt=""Janiak-Meble"" width=""262"" height=""120"" /></a></p>
                    <p><strong>Drogi kliencie,&nbsp;<br /></strong>otrzymujesz tą wiadomość, ponieważ skorzystałeś z formularza kontaktowego na naszej stronie.&nbsp;<br /><br /></p>
                    <p><strong>Treść Twojego zapytania:</strong><br />{contactForm?.Message}<br /><br />
                    <strong>Dane które otrzymaliśmy od Ciebie:<br /></strong>
                    {name}
                    {email}
                    {phone}              
                    <p><br /><br />Odpowiemy na Twoje pytanie najszybciej jak to możliwe. Jeśli nie otrzymasz odpowiedzi <span style=""color: #008000;"">możesz odpowiedzieć na tą wiadomość.</span></p>
                    <p>Pozdrawiamy<br />Janiak-Meble</p>";
        }

        private static string GenerateMessageBodyToOffice(ContactFormModel contactForm)
        {
            var name = !string.IsNullOrWhiteSpace(contactForm.Name) ? $"Imię: {contactForm.Name}<br />" : "";
            var email = !string.IsNullOrWhiteSpace(contactForm.Email) ? $"Adres email: {contactForm.Email}<br />" : "";
            var bigEmail = !string.IsNullOrWhiteSpace(contactForm.Email) ? $"<a href=\"mailto:{contactForm.Email}\"><strong>{contactForm.Email}</strong></a><br /><br />" : "";
            var bigPhone = !string.IsNullOrWhiteSpace(contactForm.Phone) ? $"<strong>{contactForm.Phone}</strong><br /><br />" : "";
            var phone = !string.IsNullOrWhiteSpace(contactForm.Phone) ? $"Numer telefonu: {contactForm.Phone}<br />" : "";           
            var product = !string.IsNullOrWhiteSpace(contactForm.Metadata) ? $"<p><strong>Produkt z kt&oacute;rego zostało wysłane zapytanie:</strong><br />{contactForm.Metadata}<br /><br />" : "";

            return $@"<p><a href=""https://janiakmeble.pl""><img style=""display: block; margin-left: auto; margin-right: auto;"" src=""https://janiakmeble.pl/img/logo-min.png"" alt=""Janiak-Meble"" width=""262"" height=""120"" /></a></p>
                    <p><strong>Nowa wiadomość od klienta. <br /></strong>
                    Otrzymujesz tą wiadomość, ponieważ ktoś skorzystał z formularza kontaktowego na naszej stronie. <br /><br /></p>
                    {bigEmail}
                    {bigPhone}
                    <p><strong>Treść zapytania:</strong><br />{contactForm?.Message}<br /><br />
                    <strong>Dane które zostały podane:<br /></strong>
                    {name}
                    {email}
                    {phone}                               
                    {product}
                    <p><span style=""color: #ff0000;"">Nie odpowiadaj na tą wiadomość, tylko wyślij nową na adres podany przez klienta!</span></p>
                ";
        }

        public void SendEmailToOffice(ContactFormModel contactForm)
        {
            
            MailAddress from = new MailAddress("info@janiakmeble.pl");
            var message = GenerateMessageBodyToOffice(contactForm);

            MailMessage mail = new MailMessage();

            var smtp = new SmtpClient
            {
                Host = "janiakmeble.nazwa.pl",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("info@janiakmeble.nazwa.pl", "JMinfo123")
            };

            mail.From = from;
            mail.To.Add("biuro@janiakmeble.pl");
            mail.Subject = "Nowa wiadomość od klienta " + contactForm.Name;
            mail.Body = message;
            mail.IsBodyHtml = true;
            smtp.Send(mail);           

        }
    }
}
