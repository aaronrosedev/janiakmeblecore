﻿using JaniakMebleWebsite.Interfaces.Repositories;
using JaniakMebleWebsite.Interfaces.Services;
using JaniakMebleWebsite.Models;
using System.Collections.Generic;
using System.Linq;

namespace JaniakMebleWebsite.Services
{
    public class ContentService : IContentService
    {
        private readonly IRepository _repository;
        private static List<string> _sloganList;

        private static object __lock = new object();

        public ContentService(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Inspiration> GetInspirations()
        {
            return _repository.GetInspirations();
        }

        public Information GetInformation(string normalizeHeader)
        {
            return _repository.GetInformations().FirstOrDefault(A => A.SeoUrl.Equals(normalizeHeader));
        }

        public IEnumerable<Information> GetInformations()
        {
            return _repository.GetInformations();
        }

        public IEnumerable<Baner> GetBaners()
        {
            return _repository.GetBaners();
        }

        public IEnumerable<QuestionAndAnswer> GetQuestionAndAnswers()
        {
            return _repository.GetQuestionAndAnswers();
        }

        public void PrepareCache()
        {
            _repository.PrepareCache();
        }

        public string GetSlogan()
        {
            if (_sloganList == null)
                _sloganList = _repository.GetSlogans().ToList();

            string slogan;
            // slogan rotation 
            lock (__lock)
            {
                 slogan = _sloganList.First();
                _sloganList.Remove(slogan);
                _sloganList.Add(slogan);
            }

            return slogan;
        }
    }
}
