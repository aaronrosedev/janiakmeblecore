﻿using JaniakMebleWebsite.Interfaces.Repositories;
using JaniakMebleWebsite.Interfaces.Services;
using JaniakMebleWebsite.Models;
using System.Collections.Generic;

namespace JaniakMebleWebsite.Services
{
    public class ProductService : IProductService
    {
        private readonly IRepository _repository;

        public ProductService(IRepository repository)
        {
            _repository = repository;
        }

        public Product GetProduct(string id)
        {
            return _repository.GetProduct(id);
        }

        public Product GetProduct(int id)
        {
            return _repository.GetProduct(id);
        }

        public IEnumerable<Product> GetProductsFromCategory(string category)
        {
            return _repository.GetProducts(category);
        }

        public IEnumerable<Product> GetProductsFromCategory(int category)
        {
            return _repository.GetProducts(category);
        }
    }
}
