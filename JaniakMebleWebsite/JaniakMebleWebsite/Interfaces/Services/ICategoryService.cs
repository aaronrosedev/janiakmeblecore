﻿using JaniakMebleWebsite.Models;
using System.Collections.Generic;

namespace JaniakMebleWebsite.Interfaces.Services
{
    public interface ICategoryService
    {
        IEnumerable<Category> GetAllCategories(int departmentId);

    }
}
