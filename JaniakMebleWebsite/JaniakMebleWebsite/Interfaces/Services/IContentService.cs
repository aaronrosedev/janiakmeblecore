﻿using JaniakMebleWebsite.Models;
using System.Collections.Generic;

namespace JaniakMebleWebsite.Interfaces.Services
{
    public interface IContentService
    {
        IEnumerable<Inspiration> GetInspirations();

        IEnumerable<Information> GetInformations();

        IEnumerable<Baner> GetBaners();

        IEnumerable<QuestionAndAnswer> GetQuestionAndAnswers();

        string GetSlogan();

        Information GetInformation(string normalizeHeader);

        void PrepareCache();

    }
}
