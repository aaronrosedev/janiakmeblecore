﻿using JaniakMebleWebsite.Models;
using System.Collections.Generic;

namespace JaniakMebleWebsite.Interfaces.Services
{
    public interface IProductService
    {
        Product GetProduct(string id);

        Product GetProduct(int id);

        IEnumerable<Product> GetProductsFromCategory(string category);

        IEnumerable<Product> GetProductsFromCategory(int category);
    }
}
