﻿using JaniakMebleWebsite.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JaniakMebleWebsite.Interfaces.Services
{
    public interface IMailService
    {
        void SendEmailToOffice(ContactFormModel contactForm);

        void SendEmailToCustomer(ContactFormModel contactForm);
    }
}
