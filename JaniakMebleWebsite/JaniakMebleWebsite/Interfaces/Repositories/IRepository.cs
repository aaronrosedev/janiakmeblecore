﻿using JaniakMebleWebsite.Models;
using System.Collections.Generic;

namespace JaniakMebleWebsite.Interfaces.Repositories
{
    public interface IRepository
    {
        IEnumerable<Category> GetCategories(int departmentId);

        Category GetCategory(int categoryId);

        IEnumerable<Product> GetProducts(string category);

        IEnumerable<Product> GetProducts(int category);

        Product GetProduct(string product);

        Product GetProduct(int product);

        IEnumerable<Inspiration> GetInspirations();
        IEnumerable<Information> GetInformations();
        IEnumerable<Baner> GetBaners();
        IEnumerable<QuestionAndAnswer> GetQuestionAndAnswers();
        IEnumerable<string> GetSlogans();

        void PrepareCache();
        void RefreshCache();

    }
}
