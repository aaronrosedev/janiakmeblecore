﻿using JaniakMebleWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JaniakMebleWebsite.ViewModels
{
    public class IndexPageViewModel
    {
        public List<Category> Categories { get; set; }
        public List<Baner> Baners { get; set; }
    }
}
