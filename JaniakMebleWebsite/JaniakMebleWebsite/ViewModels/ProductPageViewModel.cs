﻿using JaniakMebleWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JaniakMebleWebsite.ViewModels
{
    public class ProductPageViewModel
    {
        public Product Product { get; set; }
        public List<QuestionAndAnswer> QuestionsAndAnswers { get; set; }
    }
}
