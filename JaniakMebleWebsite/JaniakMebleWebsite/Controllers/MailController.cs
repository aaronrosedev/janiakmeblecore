﻿using JaniakMebleWebsite.Interfaces.Services;
using JaniakMebleWebsite.Models.Forms;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Timers;

namespace JaniakMebleWebsite.Controllers
{
    public class MailController : Controller
    {
        private static Timer tm;
        private static bool LOCK;
        private IMailService _mailService;

        public MailController(IMailService mailService)
        {
            _mailService = mailService;
            tm = new Timer(5000);
            tm.Elapsed += new ElapsedEventHandler(Timer_Elapsed);
            tm.AutoReset = false;            
        }

        [HttpPost]
        public ActionResult Send(ContactFormModel contactForm)
        {
            if (!LOCK && ModelState.IsValid && contactForm != null && (!string.IsNullOrWhiteSpace(contactForm.Email) || !string.IsNullOrWhiteSpace(contactForm.Phone)))
            {
                BlockSending();
                try
                {
                    _mailService.SendEmailToOffice(contactForm);
                    if(!string.IsNullOrWhiteSpace(contactForm.Email))
                        _mailService.SendEmailToCustomer(contactForm);
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, message = "Błąd po stronie serwera" });
                }
                return Json(new { success = true });
            }
            else
            {
                return Json(new { success = false });
                //return new HttpRequestException(HttpStatusCode.BadRequest);
            }
        }

        private void BlockSending()
        {
            LOCK = true;
            tm.Start();          
        }

        protected void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            tm.Stop();

            try
            {
                LOCK = false;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}