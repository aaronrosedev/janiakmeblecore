﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JaniakMebleWebsite.Interfaces.Services;
using JaniakMebleWebsite.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JaniakMebleWebsite.Controllers
{



    [Route("[controller]/[action]")]
    [ApiController]
    public class ApiController : ControllerBase
    {

        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IContentService _contentService;

        public ApiController(IProductService productService, ICategoryService categoryService, IContentService contentService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _contentService = contentService;
            Initialize();
        }

        private void Initialize()
        {

        }
      
        [HttpGet]
        public IActionResult Categories()
        {
            try
            {
                var fornitureCategories = _categoryService.GetAllCategories(1);
                return Ok(fornitureCategories);
            }
            catch
            {
                return NotFound();
            } 
        }
       
        [HttpGet("{id}")]
        public IEnumerable<Product> Categories(int id)
        {
            try
            {
                var products = _productService.GetProductsFromCategory(id);
                return products;
            }
            catch
            {
                return null;
            }
        }

        [HttpGet("{id}")]
        public Product Product(int id)
        {
            try
            {
                var product = _productService.GetProduct(id);
                return product;
            }
            catch
            {
                return null;
            }
        }




    }
}