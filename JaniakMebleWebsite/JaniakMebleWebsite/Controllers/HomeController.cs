﻿using JaniakMebleWebsite.Interfaces.Services;
using JaniakMebleWebsite.Models;
using JaniakMebleWebsite.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace JaniakMebleWebsite.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IContentService _contentService;

        public HomeController(IProductService productService, ICategoryService categoryService, IContentService contentService)
        {
            _productService = productService;
            _categoryService = categoryService;
            _contentService = contentService;
            Initialize();
        }

        private void Initialize()
        {
           
        }
        
        public IActionResult Index()
        {
            var fornitureCategories = _categoryService.GetAllCategories(1);
            var baners = _contentService.GetBaners();


            ViewData["Slogan"] = _contentService.GetSlogan();
            IndexPageViewModel VM = new IndexPageViewModel
            {
                Categories = fornitureCategories.ToList(),
                Baners = baners.ToList()
            };

            return View(VM);
        }


        [Route("inspiracje")]
        public IActionResult Inspiration()
        {
            ViewData["Message"] = "Your contact page.";
            var inspirations = _contentService.GetInspirations();


            return View(inspirations);
        }

        [Route("informacje/{Id}")]
        public IActionResult Information(string Id)
        {
            var information = _contentService.GetInformation(Id);

            if (information == null)
                return RedirectToAction(nameof(Error));

            return View(information);
        }

        [Route("meble/{Id}")]
        public IActionResult Category(string Id)
        {
            var products = _productService.GetProductsFromCategory(Id).OrderBy(A=>A.Name);

            if (products.Any())
                return View(products);
            else
                return RedirectToAction(nameof(Error));
        }

        [Route("meble/produkt/{Id}")]
        public IActionResult Product(string Id)
        {
            var product = _productService.GetProduct(Id);
            var qa = _contentService.GetQuestionAndAnswers().ToList();

            return View(new ProductPageViewModel
            {
                Product = product,
                QuestionsAndAnswers = qa
            }
            );
        }

        [Route("problem")]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Route("clear_cache")]
        public IActionResult ClearCache()
        {
            _contentService.PrepareCache();
            return RedirectToAction(nameof(Index));
        }
    }
}
