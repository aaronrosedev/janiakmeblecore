﻿namespace JaniakMebleWebsite.Models
{
    public class Inspiration
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
    }
}
