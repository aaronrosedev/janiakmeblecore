﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JaniakMebleWebsite.Models
{
    public class Baner
    {
        public string Header { get; set; }
        public string Content { get; set; }
        public string RedirectUrl { get; set; }
        public string RedirectText { get; set; }
        public string PhotoUrl { get; set; }
    }
}
