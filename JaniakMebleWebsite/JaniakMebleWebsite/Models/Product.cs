﻿using System.Collections.Generic;

namespace JaniakMebleWebsite.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public string SeoUrl { get; set; }

        public Dictionary<string, string> Sizes { get; set; }

        public Dictionary<string, string> Parameters { get; set; }

        public List<string> Photos { get; set; }

        public Category Category { get; set; }

        public Subcategory Subcategory { get; set; }

        public List<string> Attributes { get; set; }

        public List<Textile> Textilies { get; set; }

        public int MainProduct { get; set; }

        public List<Product> Variants { get; set; } = new List<Product>();
    }
}
