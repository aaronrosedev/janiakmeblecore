﻿namespace JaniakMebleWebsite.Models
{
    public class Textile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
    }
}
