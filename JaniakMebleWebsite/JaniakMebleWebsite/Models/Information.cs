﻿namespace JaniakMebleWebsite.Models
{
    public class Information
    {
        public string Header { get; set; }
        public string SeoUrl { get; set; }
        public string Content { get; set; }
    }
}
