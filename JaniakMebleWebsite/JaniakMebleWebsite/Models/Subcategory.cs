﻿namespace JaniakMebleWebsite.Models
{
    public class Subcategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
