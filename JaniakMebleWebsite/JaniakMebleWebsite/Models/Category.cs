﻿namespace JaniakMebleWebsite.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string SeoUrl { get; set; }
        public string Name { get; set; }
        public string Prefix { get; set; }
        public string Photo { get; set; }
        public Department Department { get; set; }
    }
}
