﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JaniakMebleWebsite.Models
{
    public class QuestionAndAnswer
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
