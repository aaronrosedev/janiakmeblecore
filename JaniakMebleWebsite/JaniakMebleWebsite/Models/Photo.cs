﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JaniakMebleWebsite.Models
{
    public class Photo
    {
        public string Url { get; set; }
        public int Position { get; set; }
    }
}
