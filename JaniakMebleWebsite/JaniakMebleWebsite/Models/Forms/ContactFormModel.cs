﻿using System.ComponentModel.DataAnnotations;

namespace JaniakMebleWebsite.Models.Forms
{
    public class ContactFormModel
    {

        public string Name { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [MinLength(9)]
        public string Phone { get; set; }    

        public string Message { get; set; }

        public string Metadata { get; set; }
    }
}