
jQuery(document).ready(function($){

if (jQuery().quicksand) {

 	// Clone applications to get a second collection
    var $data = $("#thumbs").clone();
	
	//NOTE: Only filter on the main portfolio page, not on the subcategory pages
	$('.filter li').click(function(e) {
		$(".filter li").removeClass("active");	
		// Use the last category class as the category to filter by. This means that multiple categories are not supported (yet)
		var filterClass=$(this).attr('class').split(' ').slice(-1)[0];
        var $filteredData;
		if (filterClass === 'all') {
		    $filteredData = $data.find('.item-thumbs');
		} else {
		    $filteredData = $data.find('.item-thumbs[data-type=' + filterClass + ']');
		}
        $("#thumbs").quicksand($filteredData, {
            duration: 600,
            adjustHeight: 'none'
        });		
		
		$(this).addClass("active"); 			
		return false;
	});
	
}//if quicksand

});